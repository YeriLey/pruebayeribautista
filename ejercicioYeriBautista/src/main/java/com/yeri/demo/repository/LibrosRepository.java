package com.yeri.demo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.yeri.demo.model.Libros;

@Repository
public interface LibrosRepository extends MongoRepository<Libros, String> {
	public Libros findByNombre(String nombre);
	public List<Libros> findByCosto(String costo);
	public Libros  findByNoinventario(int noinventario);
	
}
