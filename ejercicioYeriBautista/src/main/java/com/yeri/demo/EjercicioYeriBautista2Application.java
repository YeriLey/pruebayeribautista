package com.yeri.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjercicioYeriBautista2Application {

	public static void main(String[] args) {
		SpringApplication.run(EjercicioYeriBautista2Application.class, args);
	}

}
