package com.yeri.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yeri.demo.model.Libros;
import com.yeri.demo.service.LibrosService;

@RestController
public class LibrosController {
	@Autowired
	private LibrosService librosService;
	
	@RequestMapping("/post")
	public String create(@RequestParam String nombre, @RequestParam String costo, @RequestParam int noinventario, @RequestParam String descripcion) {
		Libros l = librosService.create(nombre, costo, noinventario, descripcion);
		return l.toString();
	}
	
	@RequestMapping("/get")
	public Libros getLibros(@RequestParam int noinventario) {
		return librosService.getByNoinventario(noinventario);
	}
	@RequestMapping("/getAll")
	public List<Libros> getAll() {
		return librosService.getAll();
	}
	@RequestMapping("/put")
	public String update(@RequestParam int noinventario, @RequestParam String costo, @RequestParam String descripcion) {
		Libros l = librosService.update(noinventario,costo, descripcion);
		return l.toString();
	}

}
