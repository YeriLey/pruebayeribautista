package com.yeri.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Libros {
	@Id
	String id;
	String nombre;
	String costo;
	int noinventario;
	String descripcion;
	
	
	public Libros(String nombre, String costo, int noinventario, String descripcion){
		this.nombre = nombre;
		this.costo = costo;
		this.noinventario=noinventario;
		this.descripcion=descripcion;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getCosto() {
		return costo;
	}


	public void setCosto(String costo) {
		this.costo = costo;
	}


	public int getnoInventario() {
		return noinventario;
	}


	public void setnoInventario(int noinventario) {
		this.noinventario = noinventario;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String toString() {
		return "Nombre del libro: "+"nombre"+"Costo:"+"costo"+"No. Inventario:"+"noinventario"+"Descripcion:"+"descripcion";
	}

}
