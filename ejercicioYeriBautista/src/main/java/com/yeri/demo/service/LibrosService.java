package com.yeri.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yeri.demo.model.Libros;
import com.yeri.demo.repository.LibrosRepository;

@Service
public class LibrosService {

	@Autowired
	private LibrosRepository librosRepository;
	
	public Libros create(String nombre, String costo, int noinventario, String descripcion) {
		return librosRepository.save(new Libros(nombre, costo, noinventario, descripcion));
	}
	
	public List<Libros> getAll(){
		return librosRepository.findAll();
	}
	
	public Libros getByNoinventario(int noinventario) {
		return librosRepository.findByNoinventario(noinventario);
	}
	
	public Libros update(int noinventario, String costo, String descripcion) {
		Libros l = librosRepository.findByNoinventario(noinventario);
		l.setCosto(costo);
		l.setDescripcion(descripcion);
		return librosRepository.save(l);
	}

}
